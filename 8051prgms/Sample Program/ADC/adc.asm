		



		
		; adc using channel 0 in RA0, 
		; most significant 8 bit value in PORTD
		; 0 to 5V variable voltage is given
		; sampling done using timer0 delay
		
		include "p16f877a.inc"
		
		org 0x00
		goto main
		org 0x10
main:
		banksel TRISD
		clrf TRISD			; PORTD is output port
		
		banksel ADCON0
		movlw b'01000001'		; conversion clk F0sc/8, ch0, ADC on
		movwf ADCON0
	
		banksel OPTION_REG
		movlw b'10000111'		; prescale 1:256 for timer0, pull up off
		movwf OPTION_REG

		banksel ADCON1
		movlw b'00000010'		; result left justified, AN0 pin analog
        	movwf ADCON1

		banksel PORTD			
		clrf PORTD			; initially PORTD is 0
		
		bcf INTCON, TMR0IF		; clear timer0 overflow flag

loop:
		btfss INTCON, TMR0IF		; check for timer0 overflow flag
		goto loop
		bsf ADCON0, GO			; start conversion in AN0, RA0
wait:
		btfss PIR1, ADIF		; Check A/D conversion over Interrupt flag
		goto wait
		
		movf ADRESH, 0			; move most significant 8 bit to w
		movwf PORTD			; display it in PORTD
		
		bcf PIR1, ADIF			; clear A/D Interrupt flag
		
		bcf INTCON, TMR0IF		; clear timer0 overflow flag
		
		goto loop

		end


		
		

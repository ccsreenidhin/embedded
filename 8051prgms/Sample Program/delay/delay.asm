		; generating software delay
		; PortD is used as output
		include "p16f877a.inc"
		org 0x00
		goto main
		org 0x10
main:   
		banksel TRISD
		movlw 0x00		; Port D is configured as Output Port
		movwf TRISD
		banksel PORTD
		movlw b'00000000'	; initially 0 is written in Port D
		movwf PORTD
		

repeat:
		call delay		; delay is a subroutine
		incf PORTD,1		; after the delay, Port D is incremented
		goto repeat		; and incremented value is written in Port D
					; the above steps are repeated
delay:
		movlw 0xff		; nested loop, outer loop
		movwf 0x21
d1:		movlw 0xff		; inner loop
		movwf 0x22

d2:		
		nop			; one instruction cycle delay is added
		nop
		decfsz 0x22,1		; decrement content of 0x22 location
					; if zero, skip the next line
		goto d2
		decfsz 0x21,1
		goto d1
		return

		end


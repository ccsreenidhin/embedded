		; extint
		; RB0 is used as external Interrupt
		; for every external interrupt
		; RD) LED is made on for a delay and switched off  
		include "p16f877a.inc"
		org 0x00
		goto main
		org 0x04
		goto isr
		org 0x10
main:
		banksel TRISD
		clrf TRISD		; PortD is output port
		movlw b'11111111'
		movwf OPTION_REG	; weak pull up off, rising edge RB0
		movlw b'00000001'
		movwf TRISB		; RB0 as input pin
		
		bsf INTCON, INTE	; external interrupt enable
		bsf INTCON, GIE		; global interrupt enable
		bcf INTCON, INTF	; external interrupt flag reset
		
		banksel PORTD
		clrf PORTD
loop:
		goto loop

isr:
		bcf INTCON, INTF	; external interrupt flag reset
		bsf PORTD,0
		call delay
		bcf PORTD,0
		retfie
delay:
		movlw 0x08		; nested loop, outer loop
		movwf 0x24
d1:		movlw 0xff		; inner loop
		movwf 0x25
d2:    		movlw 0xff		; inner loop
		movwf 0x26

d3:		
		nop			; one instruction cycle delay is added
		nop
		decfsz 0x26,1		; decrement content of 0x22 location
					; if zero, skip 	the next line
		goto d3
		decfsz 0x25,1
		goto d2
		decfsz 0x24,1
		goto d1
		return		
	
		end
		
		

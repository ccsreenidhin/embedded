		; multiply one 8 bit no.(B) with another 8 bit no.(A)
		; multiplication is carried out by series of addition
		; A is stored in 0x20h  
		; B is stored in 0x21h
		; low 8 bit result is stored in 0x22h
		; high 8 bit result is stored in 0x23h
		; Port D is sued to display the low 8 bit result
		; after a delay, the same PORTD is used to display high 8 bit result
		; 		
		include "p16f877a.inc"
		
		org 0x00
		goto main
		org 0x10
main
		banksel TRISD
		clrf TRISD		; make Port D as output port
		clrf TRISC		; make PORTC as output port
		banksel PORTC
		clrf PORTC		; initially make zero
		clrf PORTD		; initailly make zero

		movlw 0x75		; take a hex number 75  -- A
		movwf 0x20		; store it in x20 memory location
		movlw 0x03		; take a hex number 03	-- B
		movwf 0x21		; store it in 0x21 location
		
multiply
		clrw			; clear W reg. It is the accumulator
		clrf 0x22		; low order result location
		clrf 0x23		; high order result location
		
add
		bcf STATUS,C		; clear carry
		movf 0x22,0		; move the partial result to W reg.			
		addwf 0x20,0		
		movwf 0x22		; the result is again stored in 0x22
		btfss STATUS,C		; if carry,skip the next instruction
		goto repeat
		incf 0x23,1		; if carry,add 1 to higherorder result
		
repeat
		decfsz 0x21,1		; decrement less value (B)
					; skip next line if the value becomes 0
					; otherwise write the decremented value in 
					; same location
		goto add		; if the decremented value(B)is not 0,then 
					; A is added with W reg.
		goto disp		; when the decremented value(B)is zero,then
					; the results are displayed



disp
		movf 0x22,0		; bring 0x22 content to W reg.
		movwf PORTD
		call delay
		clrw 
		movwf PORTD
		call delay
		movf 0x23,0		; bring 0x23 content to W reg.
		movwf PORTD
		call delay
		clrw 
		movwf PORTD
		call delay
		goto disp

delay:
		movlw 0x08		; nested loop, outer loop
		movwf 0x24
d1:		movlw 0xff		; inner loop
		movwf 0x25
d2:     	movlw 0xff		; inner loop
		movwf 0x26

d3:		
		nop			; one instruction cycle delay is added
		nop
		decfsz 0x26,1		; decrement content of 0x22 location
					; if zero, skip 	the next line
		goto d3
		decfsz 0x25,1
		goto d2
		decfsz 0x24,1
		goto d1
		return

		end
	
		; try A as 0x25, B as 0x05 and see the results
		; try A as 0xEA, B as 0x4A and see the results 
		; note down the observation	
	

		

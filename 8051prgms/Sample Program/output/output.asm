		;output.asm
		; to display 34h in PortD
		include "p16f877a.inc"

		org 0x00
		goto main
		org 0x10
main:
		banksel TRISD
		movlw 0x00	; Port D is configured as output port
		movwf TRISD
		banksel PORTD
		clrf PORTD	; initially Port D is made 0
		movlw 0x34	; 0x34 is written in Port D
		movwf PORTD
		goto $		; halt
		end

	

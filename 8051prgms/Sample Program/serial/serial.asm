		; serial communication, tranmit 3 characters
		; 9600 baud, 8 bit data, no parity, 1 stop bit
		
		include "p16f877a.inc"
		org 0x00
		goto main		
		org 0x04		; no isr is used
		retfie
		org 0x10
main:
		banksel TRISC
		movlw b'10000000' 	; RC7 (RX) as input
					; RC6 (TX) as output
		movwf TRISC
		banksel SPBRG		
		movlw d'129'		; 9600 baud for 20 MHz clock
		movwf SPBRG		
		banksel RCSTA
		movlw b'10000000'	; SPEN = 1 to define 
					; RC6 and RC7 as serial port 
		movwf RCSTA
		banksel TXSTA
		bsf TXSTA, BRGH		; High speed baud rate
		bsf TXSTA, TXEN		; enable transmission

; transmit a character
		movlw a'A'		; enter the data in ASCII format
		banksel TXREG		
		movwf TXREG		; place the data in Transmit register
		call transmit		; Tranmit sub routine is called
				
		movlw a'B'
		banksel TXREG
		movwf TXREG
		call transmit

		movlw a'C'
		banksel TXREG
		movwf TXREG
		call transmit

		goto $			; halt
transmit:
		banksel TXSTA		
		btfss TXSTA, TRMT	; check for TXREG empty
		goto transmit
		call delay		; between characters, small delay
		return				

delay:
		movlw 0xff
		movwf 0x22
d1:
		movlw 0xff
		movwf 0x21
d2:
		decfsz 0x21,1
		goto d2
		decfsz 0x22,1
		goto d1
		return
		end

							

		; subtract one 8 bit no.(B) from another 8 bit no.(A)
		; A is stored in 0x20h , 
		; B is stored in 0x21h
		; 8 bit result is stored in 0x22h
		; borrow is stored in 0x23h
		; Port D is used to display the 8 bit result
		; if the result is positive, borrow is 1
		; if the result is negative, borrow is 0 and the result is 
		; in 2's complement form
		; RC2 bit is used to display the borrow
		; RC2 jumper is in the LED side
		
include "p16f877a.inc"
		
		org 0x00
		goto main
		org 0x10
main
		banksel TRISD
		clrf TRISD			; make Port D as output port
		clrf TRISC			; make PORTC as output port
		banksel PORTC
		clrf PORTC			; initially make zero
		clrf PORTD			; initailly make zero

		movlw 0x50			; take a hex number 50  -- A
		movwf 0x20			; store it in x20 memory location
		movlw 0x1A			; take a hex number 1A	-- B
		movwf 0x21			; store it in 0x21 location
		subwf 0x20,0			; Hex no. 1A is in W reg.
						; It is sutracted from 50H
						; result is in W reg.
		movwf 0x22			; 8 bit result is stored in 0x22h
		movwf PORTD			; it is displayed in PORTD
		btfss STATUS,C			; borrow status is checked
		goto stop			; if no borrow, it jumps to stop label 
		movlw 0x01			; if there is borrow, 1 is stored in 0x23h
		movwf 0x23
		movlw 0x04			; to indicate in LED (RC2) 04H is  
		movwf PORTC			; written in PORTC
stop:
		goto stop			; halt
		end
	
		; try A as 0xA0, B as 0x4A and see the results
		; try A as 0x4A, B as 0xA0 and see the results 
		; note down the observation	


		

		;
		;  generating dealy 
		;  using timer0 and count variable
		;  Port D is incremented for every 16 overflow
		;  to increase the delay, the count variable is used
		
		include "p16f877a.inc"

cblock	0x20				; variable can be stored from 0x20 location
		count			; the variable name is count
endc
		org 0x00
		goto main
		org 0x10
main:
		banksel OPTION_REG
		movlw b'10000111'	; Port B weak pull up disabled
					; timer0 prescale is selected as 1:256
		movwf OPTION_REG
		movlw b'00000000'	; Port D is configured as output port
		movwf TRISD		
		
		banksel PORTD
		clrf PORTD		; initally Port D is made 0
		
		movlw b'00000000'	; timer0 preset value is 0
		movwf TMR0
loop:		
		movlw 0x0F		; initally count reg. is made as 0x0f
		movwf count
loop1:
		bcf INTCON, TMR0IF	; initally timer0 overflow flag is reset

loop2:	
		btfss INTCON, TMR0IF	; timer0 overflow bit is checked
		goto loop2		; if not, again check
		decfsz count,1		; if yes, decrement count reg. value
		goto loop1		; if not zero, 
		incf PORTD,1		; increment Port D
		goto loop		; load again count reg 0x0f and repeat

		end




		


		;
		; timer1 using interrupt
		; increments the Port D
		
		include "p16f877a.inc"
		
		org 0x00
		goto main
		
		org 0x04
		goto isr
		
		org 0x10
main:
		banksel TRISD
		clrf TRISD		; Port D is output port

		banksel T1CON
		movlw b'00110101'	; prescale 1:8, async, internal clock, on
		movwf T1CON

		banksel PIE1		
		bsf PIE1, TMR1IE	; enable timer1 interrupt 
		
		banksel PORTD
		clrf PORTD		; initially PORT D is 0
		
		clrf TMR1L		; preset value of timer is 0	
		clrf TMR1H			
		
		bsf INTCON, PEIE	; peripheral interrupt enable
		bsf INTCON, GIE		; global interrupt enable
		
		bsf PIR1, TMR1IF	; initially clear the overflow flag of timer1

		goto $

isr:	
		call timer1		; the subroutine is to clear the overflow flag and
					; to increment the Port D
		retfie

timer1:
		bcf PIR1, TMR1IF	; reset the timer1 interrupt flag reset
		incf PORTD,1
		return

		end



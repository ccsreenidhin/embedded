		




		; use of timer2
		; period register is loaded with 249.  
		; interrupt is used to increment PORTD
		
		include "p16f877a.inc"
		
		org 0x00
		goto main
		
		org 0x04
		goto isr
		
		org 0x10
main:
		banksel TRISD
		clrf TRISD		; Port D is Output port
		movlw d'249'
		movwf PR2		; 249 is written in Period Reg. of Timer2
		
		banksel T2CON
		movlw b'01111110'	; prescale 1:16, Post scale 1:16, on
		movwf T2CON
		
		banksel PIE1
		bsf PIE1, TMR2IE	; Timer2 interrupt enable
		
		banksel PORTD
		clrf PORTD		; Initially Port D is 0
		
		clrf TMR2		; 8 bit Timer2 reg.Preset value is 0
		
		bsf INTCON, PEIE	; Peripheral interrupt is enabled
		bsf INTCON, GIE		; global interrupt is enabled

		goto $

isr:  
		call timer2
		retfie
timer2:
		bcf PIR1, TMR2IF	; Timer2 interrupt flag is reset
		incf PORTD,1
		return

		end



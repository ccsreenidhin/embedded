		;
		;  using timero interrupt
		;  increment PORTD for every interrupt
		include "p16f877a.inc"
		org 0x00
		goto main
		
		org 0x04		; Interrupt vectoraddress
		goto isr
		
		org 0x10
main:
		banksel TRISD
		clrf TRISD		; PortD is output port
		movlw b'10000111'	
		movwf OPTION_REG 	; timer0 prescalar 1:256
		
		banksel PORTD
		clrf PORTD		; initially Port D is made 0
		
		clrf TMR0		; timer preset value is 0
		
		bsf INTCON, TMR0IE  	; timer0 interrupt enable
		bsf INTCON, GIE		; enable global interrupt
loop:
		goto loop		; halt

isr:					; whenever timer0 overflow occurs, 
					; it gives interrupt
	
					; interrupt service routine
		bcf INTCON, TMR0IF	; reset the overflow flag
		incf PORTD,1		; increment Port D
		retfie			; return from the interrupt service routine

		end


	
